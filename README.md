# LinkedOut

Alternative to LinkedIn -- host your own profile, using a common schema so people can "link-in" to each other without the LinkedIn platform

This literally just happened [on LinkedIn][1].

Briefly
> A possible alternative: LinkedOut,
> which looks something like this:
> - everyone hosts their own professional profile at their self-hosted site (you're doing this already, right?)
> - a common schema for professional content (the stuff we come to LinkedIn for: resume/CV)
> - a UI that allows easy input to that schema (may be third-party, may be backed into the hosted page
> - a front-end that collects new posts and comments from your connection's self-hosted sites, providing you a Feed like in LinkedIn
> 
> I'm claiming the name LinkedOut here and now (for what that's worth); whoever wants to help out can contribute here: 

UPDATE: basically adopted https://jsonresume.org/schema/ but would like to come up with a lightweight way to render it locally

# CHRP: decentralized twitter-like communications

Basic idea is that all of your chirps live on your site, and other people's clients pull your chirps directly.

Your chirps are signed by you (ideally) proving that they haven't been tampered with or placed on your site by 3rd party. If something is posted to your site that isn't signed by you, all of your followers will know it immediately (assuming your PGP keys haven't been compromised).

Basic client could be Javascript SPA, which could reach out and check all the followed rss feeds. A more advanced client could be server-side at your hosted site, doing the grunt work and providing 'local'/optimized results to your client (web based or mobile app).

By default it would be 'no algorithm', but there is no reason why some preferrential algorithm could be used as a plugin/addon. Instead of a corporate-imposed algorithm, choose your own flavor.

* overrides at .well-known/chirp.config
* /chrp/rss.xml returns an rss feed of all chirps
* /chrip/{id}/ renders an individual chirp
* /chrp/tag/rss.xml returns feed by tag


[1]: https://www.linkedin.com/feed/update/urn:li:activity:6721705325944176642?commentUrn=urn%3Ali%3Acomment%3A%28activity%3A6721705325944176642%2C6721783037257891840%29
